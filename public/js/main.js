
import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r112/build/three.module.js';
import {OrbitControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r112/examples/jsm/controls/OrbitControls.js';
import {GLTFLoader} from 'https://threejsfundamentals.org/threejs/resources/threejs/r112/examples/jsm/loaders/GLTFLoader.js';



function main() {
  const canvas = document.querySelector('#c');
  const renderer = new THREE.WebGLRenderer({canvas, alpha: true, antialiasing: true});
  renderer.physicallyCorrectLights = true;
  renderer.toneMapping = THREE.LinearToneMapping;
  renderer.setClearColor(0x000000,0);

  const fov = 70;
  const aspect = 2;  // the canvas default
  const near = 0.1;
  const far = 100;
  const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(0, 0, 0);
  camera.zoom = 5;



  const controls = new OrbitControls(camera, canvas);
  controls.target.set(0, -1, 0);
  controls.update();
  controls.autoRotate = true;
  controls.enableDamping = true;
  controls.dampingFactor = 0.05;
  controls.enablePan = false;


  let objects = [];
  let rotation = [];
  let ready = false;
  const scene = new THREE.Scene();

  var textureCube;



  THREE.DefaultLoadingManager.onLoad = function ( ) {

    pmremGenerator.dispose();

  };


  var r = "assets/textures/cube/";
  var urls = [ r + "px.png", r + "nx.png",
         r + "py.png", r + "ny.png",
         r + "pz.png", r + "nz.png" ];

  textureCube = new THREE.CubeTextureLoader().load( urls );
  textureCube.format = THREE.RGBFormat;
  textureCube.mapping = THREE.CubeReflectionMapping;
  textureCube.encoding = THREE.sRGBEncoding;





  var pmremGenerator = new THREE.PMREMGenerator( renderer );
  pmremGenerator.compileEquirectangularShader();
  renderer.outputEncoding = THREE.sRGBEncoding;


  var ambientLight = new THREE.AmbientLight( 0xAAAAAA ); // soft white light
  scene.add( ambientLight );

  

  function frameArea(sizeToFitOnScreen, boxSize, boxCenter, camera) {
    const halfSizeToFitOnScreen = sizeToFitOnScreen * 0.5;
    const halfFovY = THREE.Math.degToRad(camera.fov * .5);
    const distance = halfSizeToFitOnScreen / Math.tan(halfFovY);
    // compute a unit vector that points in the direction the camera is now
    // in the xz plane from the center of the box
    const direction = (new THREE.Vector3())
        .subVectors(camera.position, boxCenter)
        .multiply(new THREE.Vector3(1, 0, 1))
        .normalize();

    // move the camera to a position distance units way from the center
    // in whatever direction the camera was from the center already
    camera.position.copy(direction.multiplyScalar(distance).add(boxCenter));

    // pick some near and far values for the frustum that
    // will contain the box.
    camera.near = boxSize / 100;
    camera.far = boxSize * 100;

    camera.updateProjectionMatrix();

    // point the camera to look at the center of the box
    camera.lookAt(boxCenter.x, boxCenter.y, boxCenter.z);
  }

  

  {
    const gltfLoader = new GLTFLoader();
    gltfLoader.load('assets/scene_demo_static.glb', (gltf) => {
      const root = gltf.scene;
      root.scale.set(0.2, 0.2, 0.2);
      
      root.traverse ( ( o ) => {
        if ( o.isMesh ) {
          // note: for a multi-material mesh, `o.material` may be an array,
          // in which case you'd need to set `.map` on each value.
          o.material.envMap = textureCube;
          if(o.material.name == 'Vetro.001'){
            o.material.transparent = true;
            o.material.opacity = 0.75;
          }
        }
      } );
      scene.add(root);
    

      root.children.forEach((obj, index) => {
      obj.position.x = (Math.random()*200)-50;
      obj.position.y = (Math.random()*200)-50;
      obj.position.z = (Math.random()*200)-50;

      objects.push(obj);
      rotation[index] = Math.random()/75;
      });
      ready = true;


      // compute the box that contains all the stuff
      // from root and below
      const box = new THREE.Box3().setFromObject(root);
      const boxSize = box.getSize(new THREE.Vector3()).length();
      const boxCenter = box.getCenter(new THREE.Vector3());

      // set the camera to frame the box
      frameArea(boxSize * 1, boxSize, boxCenter, camera);

      // update the Trackball controls to handle the new size
      controls.maxDistance = boxSize * 10;
      controls.target.copy(boxCenter);
      controls.maxDistance = 120;
      controls.minDistance = 50;
      controls.update();
    });
  }

  function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {
      renderer.setSize(width, height, false);
    }
    return needResize;
  }



  function render() {
    if (resizeRendererToDisplaySize(renderer)) {
      const canvas = renderer.domElement;
      camera.aspect = canvas.clientWidth / canvas.clientHeight;
      camera.updateProjectionMatrix();
    }
    if(ready){
    objects.forEach((obj, index) => { 
      obj.rotation.y += rotation[index];
      obj.rotation.x += rotation[index];
    });
  }
  controls.update();

  renderer.toneMappingExposure = 3;
    renderer.render(scene, camera);

    requestAnimationFrame(render);
  }
  
  requestAnimationFrame(render);
}

main();